package com.transfert.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="banque")
public class Banque implements java.io.Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idBanque")
	private int idBanque;
	
	@Column(name="nom")
	private String nom;
	
	@ManyToOne
    @JoinColumn(name="banque_pays", nullable=false)
	private Pays pays;
	
	@ManyToMany(mappedBy = "banques")
	private Set<Client> clients = new HashSet<Client>();
	
	@OneToMany(mappedBy="banque")
	private Set<TransfertBanque> transfertbanques;
	
	
	
	public Banque() {
	}

	
	public int getIdBanque() {
		return idBanque;
	}

	public void setIdBanque(int idBanque) {
		this.idBanque = idBanque;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	
	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}
	
	
	public Set<Client> getClients() {
		return clients;
	}

	public void setClients(Set<Client> clients) {
		this.clients = clients;
	}

	
	public Set<TransfertBanque> getTransfertbanques() {
		return transfertbanques;
	}

	public void setTransfertbanques(Set<TransfertBanque> transfertbanques) {
		this.transfertbanques = transfertbanques;
	}
	
	

	
}
