package com.transfert.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="pays")
public class Pays implements java.io.Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPays")
	private int idPays;
	
	@Column(name="nom")
	private String nom; 
	
	@OneToMany(mappedBy="pays")
	private Set<Banque> banques;
	
	@OneToMany(mappedBy="pays")
	private Set<Beneficiaire> beneficiares;
	
	@OneToOne(mappedBy="pays", cascade=CascadeType.ALL)
	private TauxTransfert tauxtransfert;
	
	

	public Pays() {
	}

	public Pays(int idPays, String nom) {
		super();
		this.idPays = idPays;
		this.nom = nom;
	}

	
	public int getIdPays() {
		return idPays;
	}

	public void setIdPays(int idPays) {
		this.idPays = idPays;
	}
	
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	
	public Set<Banque> getBanques() {
		return banques;
	}

	public void setBanques(Set<Banque> banques) {
		this.banques = banques;
	}
	
	
	public Set<Beneficiaire> getBeneficiares() {
		return beneficiares;
	}

	public void setBeneficiares(Set<Beneficiaire> beneficiares) {
		this.beneficiares = beneficiares;
	}
	
	
	public TauxTransfert getTauxtransfert() {
		return tauxtransfert;
	}

	public void setTauxtransfert(TauxTransfert tauxtransfert) {
		this.tauxtransfert = tauxtransfert;
	}
	
}
