package com.transfert.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="recutransf")
public class RecuTransf  implements java.io.Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRecuTransf")
	private int idRecuTransf;
	
	@Column(name="date")
	private Date date;
	
	
	public RecuTransf() {
		
	}
	public RecuTransf(int idRecuTransf, Date date) {
		super();
		this.idRecuTransf = idRecuTransf;
		this.date = date;
	}
	
	public int getIdRecuTransf() {
		return idRecuTransf;
	}
	public void setIdRecuTransf(int idRecuTransf) {
		this.idRecuTransf = idRecuTransf;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
}
