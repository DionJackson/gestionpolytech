package com.transfert.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="transfert")
public class Transfert implements java.io.Serializable{

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idTransfert")
	private int idTransfert;
	
	@Column(name="montant")
	private Double montant;
	
	@Column(name="sommeRecev")
	private Double sommeRecev;
	
	@ManyToOne
	@JoinColumn(name="client_id", nullable=false)
	private Client client;
	
	@OneToMany(mappedBy="transfert")
	private Set<Recu> recus;
	
	public Transfert() {
	}

	public Transfert(int idTransfert, Double montant, Double sommeRecev) {
		super();
		this.idTransfert = idTransfert;
		this.montant = montant;
		this.sommeRecev = sommeRecev;
	}

	
	public int getIdTransfert() {
		return idTransfert;
	}

	public void setIdTransfert(int idTransfert) {
		this.idTransfert = idTransfert;
	}

	
	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	public Double getSommeRecev() {
		return sommeRecev;
	}
	
	public void setSommeRecev(Double sommeRecev) {
		this.sommeRecev = sommeRecev;
	}

	public Client getClient() {
		return client;
	}

	public void setClients(Client client) {
		this.client = client;
	}
	
	
	public Set<Recu> getRecus() {
		return recus;
	}

	public void setRecus(Set<Recu> recus) {
		this.recus = recus;
	}
	
}
