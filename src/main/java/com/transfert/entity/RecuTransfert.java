package com.transfert.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="recutransfert")
public class RecuTransfert  implements java.io.Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idRecuTransfert")
	private int idRecutransfert;
	
	@ManyToOne
	@JoinColumn(name="recutransfert_id", nullable=false)
	private TransfertBanque transfertbanque;
	
	@Column(name="date")
	private Date date;
	
	
	
	public RecuTransfert(int idRecutransfert, TransfertBanque transfertbanque) {
		super();
		this.idRecutransfert = idRecutransfert;
		this.transfertbanque = transfertbanque;
	}
	
	
	public RecuTransfert() {
		
	}

	
	public int getIdRecutransfert() {
		return idRecutransfert;
	}
	public void setIdRecutransfert(int idRecutransfert) {
		this.idRecutransfert = idRecutransfert;
	}
	
	public TransfertBanque getTransfertbanque() {
		return transfertbanque;
	}
	public void setTransfertbanque(TransfertBanque transfertbanque) {
		this.transfertbanque = transfertbanque;
	}

	
	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}
	
	

}
