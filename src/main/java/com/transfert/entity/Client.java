package com.transfert.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "client", catalog = "apptransfert")
public class Client implements java.io.Serializable {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idClient")
	private int idClient;
	
	@Column(name="nom")
	private String nom;
	
	@Column(name="prenom")
	private String prenom;
	
	@Column(name="username")
	private String username;
	
	@Column(name="password")
	private String password;
	
	@OneToMany(mappedBy="client")
	private Set<Beneficiaire> beneficiaires;
	
	
	@OneToMany(mappedBy="client")
	private Set<Transfert> transferts;
	
	@ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
        name = "Banque_client", 
        joinColumns = { @JoinColumn(name = "banque_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "client_id") }
    )
	private Set<Banque> banques = new HashSet<>();
	
	

	public Client() {
	}

	public Client(int idClient, String nom, String prenom, String username, String password) {
		super();
		this.idClient = idClient;
		this.nom = nom;
		this.prenom = prenom;
		this.username = username;
		this.password = password;
	}

	
	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public Set<Beneficiaire> getBeneficiaires() {
		return beneficiaires;
	}

	public void setBeneficiaires(Set<Beneficiaire> beneficiaires) {
		this.beneficiaires = beneficiaires;
	}
	
	public Set<Transfert> getTransferts() {
		return transferts;
	}

	public void setTransferts(Set<Transfert> transferts) {
		this.transferts = transferts;
	}
	
	
	public Set<Banque> getBanques() {
		return banques;
	}

	public void setBanques(Set<Banque> banques) {
		this.banques = banques;
	}
	
}
