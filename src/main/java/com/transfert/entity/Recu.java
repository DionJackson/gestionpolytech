package com.transfert.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="recu")
public class Recu  implements java.io.Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idRecu")
	private int idRecu;
	
	@ManyToOne
    @JoinColumn(name="recu_transfert", nullable=false)
	private Transfert transfert;

	
	public int getIdRecu() {
		return idRecu;
	}

	public void setIdRecu(int idRecu) {
		this.idRecu = idRecu;
	}

	
	public Transfert getTransfert() {
		return transfert;
	}

	public void setTransfert(Transfert transfert) {
		this.transfert = transfert;
	}
	
}
