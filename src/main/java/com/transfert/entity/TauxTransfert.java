package com.transfert.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="tauxtransfert")
public class TauxTransfert  implements java.io.Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idTauxTransfert")
	private int idTauxTransfert;
	
	@Column(name="montant")
	private Double montant;
	
	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private Pays pays;
	
	
	public TauxTransfert() {
		
	}

	public TauxTransfert(int idTauxTransfert, Double montant) {
		super();
		this.idTauxTransfert = idTauxTransfert;
		this.montant = montant;
	}
	
	public int getIdTauxTransfert() {
		return idTauxTransfert;
	}

	public void setIdTauxTransfert(int idTauxTransfert) {
		this.idTauxTransfert = idTauxTransfert;
	}
	
	public Double getMontant() {
		return montant;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}
	
	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}
	
}
