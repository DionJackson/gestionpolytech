package com.transfert.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="transfert_banque")
public class TransfertBanque  implements java.io.Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idtransfert_banque")
	private int idTransfertBanque;
	
	@ManyToOne
    @JoinColumn(name="TransfertBanque_id", nullable=false)
	private Banque banque;
	
	@ManyToOne
	private Beneficiaire beneficiaire;
	
	@OneToMany(mappedBy="transfertbanque")
	private Set<RecuTransfert> recutransfertbanques;
	
	
	public int getIdTransfertBanque() {
		return idTransfertBanque;
	}
	public void setIdTransfertBanque(int idTransfertBanque) {
		this.idTransfertBanque = idTransfertBanque;
	}
	
	
	public Banque getBanque() {
		return banque;
	}
	public void setBanque(Banque banque) {
		this.banque = banque;
	}
	
	/*
	public Beneficiaire getBeneficiaire() {
		return beneficiaire;
	}
	public void setBeneficiaire(Beneficiaire beneficiaire) {
		this.beneficiaire = beneficiaire;
	}
	*/
	public Set<RecuTransfert> getRecutransfertbanques() {
		return recutransfertbanques;
	}
	public void setRecutransfertbanques(Set<RecuTransfert> recutransfertbanques) {
		this.recutransfertbanques = recutransfertbanques;
	}
	

}
