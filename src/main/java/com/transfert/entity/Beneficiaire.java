package com.transfert.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name ="beneficiaire")
public class Beneficiaire implements java.io.Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idBenefice")
	private int idBeneficiare;
	
	@ManyToOne
    @JoinColumn(name="client_beneficiaire", nullable=false)
	private Client client;
	
	@ManyToOne
    @JoinColumn(name="pays_beneficiaire", nullable=false)
	private Pays pays;
	
	 @OneToMany(mappedBy="beneficiaire")
	 private Set<TransfertBanque> transfertbanque;

	
	public int getIdBeneficiare() {
		return idBeneficiare;
	}

	public void setIdBeneficiare(int idBeneficiare) {
		this.idBeneficiare = idBeneficiare;
	}

	
	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	
	public Pays getPays() {
		return pays;
	}

	public void setPays(Pays pays) {
		this.pays = pays;
	}
	
	/*
	public Set<TransfertBanque> getTransfertbanque() {
		return transfertbanque;
	}

	public void setTransfertbanque(Set<TransfertBanque> transfertbanque) {
		this.transfertbanque = transfertbanque;
	}
	
	*/

}
