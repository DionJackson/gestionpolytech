package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Beneficiaire;

public interface BeneficiaireRepository extends JpaRepository<Beneficiaire, Integer>{

}
