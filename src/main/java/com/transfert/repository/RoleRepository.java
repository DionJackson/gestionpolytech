package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Role;

public interface RoleRepository  extends JpaRepository<Role, Integer>{

}
