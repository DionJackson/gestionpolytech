package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Pays;

public interface PaysRepository  extends JpaRepository<Pays, Integer>{

}
