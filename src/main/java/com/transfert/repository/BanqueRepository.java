package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.transfert.entity.Banque;

public interface BanqueRepository  extends JpaRepository<Banque, Integer>{

}
