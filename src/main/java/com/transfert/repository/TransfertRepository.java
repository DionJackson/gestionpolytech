package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Transfert;

public interface TransfertRepository extends JpaRepository<Transfert, Integer>{

}
