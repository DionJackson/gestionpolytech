package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Client;

public interface ClientRepository extends JpaRepository<Client, Integer>{

}
