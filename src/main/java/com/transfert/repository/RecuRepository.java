package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Recu;

public interface RecuRepository extends JpaRepository<Recu, Integer>{

}
