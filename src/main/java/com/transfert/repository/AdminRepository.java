package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.Admin;

public interface AdminRepository  extends JpaRepository<Admin, Integer>{

}
