package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.RecuTransf;

public interface RecuTransfRepository extends JpaRepository<RecuTransf, Integer>{

}
