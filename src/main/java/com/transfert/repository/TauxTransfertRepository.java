package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.TauxTransfert;

public interface TauxTransfertRepository extends JpaRepository<TauxTransfert, Integer>{

}
