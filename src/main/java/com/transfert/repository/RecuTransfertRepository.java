package com.transfert.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.transfert.entity.RecuTransfert;

public interface RecuTransfertRepository  extends JpaRepository<RecuTransfert, Integer>{

}
