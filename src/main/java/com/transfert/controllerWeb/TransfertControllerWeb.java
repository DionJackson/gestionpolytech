package com.transfert.controllerWeb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.transfert.repository.TransfertRepository;
import com.transfert.service.TransfertService;

@Controller
public class TransfertControllerWeb {

	@Autowired
	private TransfertService service;
	
	@RequestMapping("/transfert/all")
	public String AllTransfert(Model model) {
		
		return "TransfertTemplate/nouveauTransfert";
	}
}
