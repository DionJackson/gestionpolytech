package com.transfert.controllerWeb;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.transfert.entity.Admin;
import com.transfert.entity.Role;
import com.transfert.service.AdminService;
import com.transfert.service.RoleService;

@Controller
public class AdminControllerWeb {
	
	@Autowired
	private AdminService service;
	
	@Autowired
	private RoleService servRole;
	
	
	@RequestMapping("/admin/allAdmin/")
	public String connexionAdmin(Model model) {
	List<Admin> admins = service.getAllAdmin();
	//List<Role> roles = servRole.getAllRole();
	model.addAttribute("admin", new Admin());
		return "AdminTemplate/listAdmin";
	}
	
	@RequestMapping("/admin/new/")
	public String findAllAdmin(Model model) {
	 Admin admin = new Admin();
	 model.addAttribute("admin", admin);
	 //model.addAttribute(roles, servRole.getAllRole());
		return "AdminTemplate/creerAdmin";
	}
	
	@RequestMapping(value="admin/saveAdmin/", method = RequestMethod.POST)
	public String saveAdmin(Model model, @ModelAttribute("admin") Admin admin) {
		service.saveAdmin(admin);
		return "redirect:/admin/allAdmin";
	}
	
	@GetMapping("admin/editeAdmin/{id}")
	public String editAdmin(Model model, @PathVariable("id") int id) {
		//model.addAttribute("roles", servRole.getAllRoles());
		model.addAttribute("admin", service.getByIdAdmin(id));
		return "AdminTemplate/editAdmin";
	}
	
	@PostMapping("admin/editAdmin/")
	public String UpdateAdmin(@ModelAttribute("admin") Admin admin) {
		service.updateAdmin(admin);
		return "redirect:/admin/allAdmin/";
	}
	
	@RequestMapping("admin/deleteAdmin/{id}")
	public String supprimeAdmin(Model model, @PathVariable("id") int id) {
		service.deleteAdmin(id);
		return "redirect:/admin/allAdmin/";
	}
}