package com.transfert.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.transfert.repository.AdminRepository;

import com.transfert.entity.*;

@Service
public class AdminService {

	@Autowired
	private AdminRepository repository;
	
	public Admin saveAdmin(Admin Admin) {
		return repository.save(Admin);
	}
	
	public List<Admin> saveAdmins(List<Admin> Admins){
		return repository.saveAll(Admins);
	}
	
	
	public Admin getByIdAdmin(int id) {
		return repository.findById(id).orElse(null);
	}
	
	/*
	 * public Admin getByNameAdmin(Admin name) { return
	 * repository.findByName(name); }
	 */
	
	public List<Admin> getAllAdmin(){
		return repository.findAll();
	}
	
	public Admin updateAdmin(Admin admin) {
		Admin ad = repository.findById(admin.getIdAdmin()).orElse(null);
		ad.setUsername(admin.getUsername());
		ad.setPassword(admin.getPassword());
		
		return repository.save(ad);
	}
	
	
	public String deleteAdmin(int id) {
		repository.deleteById(id);
		return "Supprimer avec succes || "+id;
	}

	public Object findByName(String nom) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/*
	 * @Override
	 * 
	 * @Cacheable("fetchAdmin") public List<Admin> fetchAdmin(String
	 * searchTerm) throws Exception { // TODO Auto-generated method stub return
	 * repository.fetch(searchTerm);
	 * 
	 * }
	 */

	
}
